#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1000 10000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1000)
        self.assertEqual(j, 10000)

    def test_read_3(self):
        s = "2000 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2000)
        self.assertEqual(j, 100)

    def test_read_4(self):
        s = "00 001\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  0)
        self.assertEqual(j, 1)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(500, 100)
        self.assertEqual(v, 144)

    def test_eval_6(self):
        v = collatz_eval(2, 2)
        self.assertEqual(v, 2)

    def test_eval_7(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_8(self):
        v = collatz_eval(500, 999500)
        self.assertEqual(v, 525)

    def test_eval_9(self):
        v = collatz_eval(1, 455555)
        self.assertEqual(v, 449)

    def test_eval_10(self):
        v = collatz_eval(999500, 999999)
        self.assertEqual(v, 290)

    def test_eval_11(self):
        v = collatz_eval(997002, 999999)
        self.assertEqual(v, 440)

    def test_eval_12(self):
        v = collatz_eval(995999, 997999)
        self.assertEqual(v, 440)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 500, 100, 144)
        self.assertEqual(w.getvalue(), "500 100 144\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 2, 3)
        self.assertEqual(w.getvalue(), "1 2 3\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_solve_3(self):
        r = StringIO("2 2\n10 10\n200 200\n1000 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 2 2\n10 10 7\n200 200 27\n1000 1000 112\n")

    def test_solve_4(self):
        r = StringIO("5 15\n300 400\n500 600\n700 800\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 15 20\n300 400 144\n500 600 137\n700 800 171\n")

    def test_solve_5(self):
        r = StringIO("3 3\n50 50\n300 300\n500 500\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 3 8\n50 50 25\n300 300 17\n500 500 111\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
