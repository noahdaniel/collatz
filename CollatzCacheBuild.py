from Collatz import collatz_eval


# Function to write results to a text file
def write_to_file(results):
    with open('collatz_range_cache.txt', 'w') as file:
        file.write('[')
        for result in results:
            file.write(str(result) + ',')
        file.write(']')

# Iterate through the specified ranges and call collatz_eval
results = []
# for i in range(1, 10000, 1000):
#     j = i + 999
#     result = collatz_eval(i, j)
#     results.append([i, j, result])

# Write the results to a text file
#write_to_file(results)
print(collatz_eval(997002, 999999))
print(collatz_eval(995999, 997999))

